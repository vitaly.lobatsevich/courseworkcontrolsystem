package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Department;

public interface DepartmentService extends CourseworkControlSystemService<Department, Long> {}
