package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "specialties")
@Data
@EqualsAndHashCode(callSuper = false)
public class Specialty extends SQLEntity {
    @NotBlank
    @Column(name = "specialty_name", nullable = false, unique = true)
    protected String name;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "faculty_id", nullable = false)
    protected Faculty faculty;

}
