package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Faculty;

public interface FacultyService extends CourseworkControlSystemService<Faculty, Long> {}
