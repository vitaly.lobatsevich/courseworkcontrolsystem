package by.vsu.courseworkcontrolsystem.entity;

import lombok.Data;

import javax.persistence.*;

@MappedSuperclass
@Data
public class SQLEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

}
