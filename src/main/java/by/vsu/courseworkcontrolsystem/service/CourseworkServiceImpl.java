package by.vsu.courseworkcontrolsystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import by.vsu.courseworkcontrolsystem.entity.Coursework;
import by.vsu.courseworkcontrolsystem.entity.User;
import by.vsu.courseworkcontrolsystem.exception.BadRequestException;
import by.vsu.courseworkcontrolsystem.repository.CourseworkRepository;
import by.vsu.courseworkcontrolsystem.repository.UserRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class CourseworkServiceImpl implements CourseworkService {
    private final CourseworkRepository courseworkRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Coursework> getAll() {
        return courseworkRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Coursework getById(final Long id) {
        return courseworkRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Coursework> getCourseworks(final String asRole, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        if (user.hasRole(asRole)) {
            if (asRole.equals("admin")) {
                return courseworkRepository.findAll();
            }
            if (asRole.equals("student")) {
                return user.getGroup().getCourseworks();
            }
            if (asRole.equals("teacher")) {
                final List<Coursework> courseworks = new ArrayList<>();
                user.getPlaces().forEach(places -> courseworks.add(places.getCoursework()));
                return courseworks;
            }
        }

        return new ArrayList<>();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Coursework coursework) {
        if (coursework.getStartOfRegistration().after(coursework.getEndOfRegistration())) {
            throw new BadRequestException("Start date cannot be later than end date");
        }
        return courseworkRepository.save(coursework).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            courseworkRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
