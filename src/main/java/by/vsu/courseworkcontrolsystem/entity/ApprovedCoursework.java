package by.vsu.courseworkcontrolsystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "approved_courseworks")
@Data
@EqualsAndHashCode(callSuper = false)
public class ApprovedCoursework extends SQLEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "student_id", nullable = false)
    @JsonIgnoreProperties({ "password", "roles", "active" })
    private User student;

    @ManyToOne
    @JoinColumn(name = "teacher_id", nullable = false)
    @JsonIgnoreProperties({ "password", "roles", "active" })
    private User teacher;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "coursework_id", nullable = false)
    @JsonIgnoreProperties("places")
    private Coursework coursework;

    @NotBlank
    @Column(name = "theme", nullable = false, unique = true)
    private String theme;

}
