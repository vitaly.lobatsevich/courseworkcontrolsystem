package by.vsu.courseworkcontrolsystem.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import by.vsu.courseworkcontrolsystem.entity.ContactInformation;
import by.vsu.courseworkcontrolsystem.entity.User;
import by.vsu.courseworkcontrolsystem.repository.ContactInformationRepository;
import by.vsu.courseworkcontrolsystem.repository.UserRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ContactInformationServiceImpl implements ContactInformationService {
    private final ContactInformationRepository contactInformationRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<ContactInformation> getAll() {
        return contactInformationRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public ContactInformation getById(final Long id) {
        return contactInformationRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final ContactInformation contactInformation) {
        return contactInformationRepository.save(contactInformation).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            contactInformationRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public ContactInformation getByUsername(final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }
        return user.getContactInformation();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long saveForUsername(final ContactInformation contactInformation, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        final ContactInformation current = user.getContactInformation();
        current.setEmail(contactInformation.getEmail());
        current.setPhoneNumber(contactInformation.getPhoneNumber());
        return contactInformationRepository.save(current).getId();
    }

}
