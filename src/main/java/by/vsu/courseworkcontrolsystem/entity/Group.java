package by.vsu.courseworkcontrolsystem.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import by.vsu.courseworkcontrolsystem.validation.GroupNumber;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "groups")
@Data
@EqualsAndHashCode(callSuper = false)
public class Group extends SQLEntity {
    @GroupNumber(message = "{group.group-number.group-number}")
    @NotBlank
    @Column(name = "group_number", nullable = false)
    private String number;

    @NotNull
    @Min(1)
    @Max(6)
    @Column(name = "course", nullable = false)
    private Integer course;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "specialty_id", nullable = false)
    @JsonIgnoreProperties("faculty")
    private Specialty specialty;

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Coursework> courseworks;

}
