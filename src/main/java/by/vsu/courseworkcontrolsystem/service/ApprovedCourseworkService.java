package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.ApprovedCoursework;

import java.util.List;

public interface ApprovedCourseworkService extends CourseworkControlSystemService<ApprovedCoursework, Long> {
    Long approve(ApprovedCoursework approvedCoursework, String username);
    List<ApprovedCoursework> getApprovedCourseworks(String asRole, String username);
}
