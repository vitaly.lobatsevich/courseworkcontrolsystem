package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Places;
import by.vsu.courseworkcontrolsystem.entity.PlacesId;

public interface PlacesService extends CourseworkControlSystemService<Places, PlacesId> {}
