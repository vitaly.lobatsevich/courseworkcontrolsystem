package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Person;

public interface PersonService extends CourseworkControlSystemService<Person, Long> {
    Person getByUsername(String username);
}
