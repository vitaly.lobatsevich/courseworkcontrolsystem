package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Group;

public interface GroupService extends CourseworkControlSystemService<Group, Long> {}
