package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.*;
import by.vsu.courseworkcontrolsystem.exception.BadRequestException;
import by.vsu.courseworkcontrolsystem.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ApprovedCourseworkServiceImpl implements ApprovedCourseworkService {
    private final ApprovedCourseworkRepository approvedCourseworkRepository;
    private final UserRepository userRepository;
    private final RequestRepository requestRepository;
    private final PlacesRepository placesRepository;
    private final CourseworkRepository courseworkRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long approve(final ApprovedCoursework approvedCoursework, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        final Request request = requestRepository.findById(
                new RequestId(
                        approvedCoursework.getStudent().getId(),
                        user.getId(),
                        approvedCoursework.getCoursework().getId()
                )).orElse(null);
        if (request == null) {
            throw new BadRequestException("No such request");
        }

        final Places places = placesRepository.findById(
                new PlacesId(
                        user.getId(),
                        approvedCoursework.getCoursework().getId())
        ).orElse(null);
        if (places == null) {
            throw new RuntimeException(
                    "Data Logic Violation: request to a teacher who does not supervise such coursework");
        }

        final User student = userRepository.findById(approvedCoursework.getStudent().getId()).orElse(null);
        if (student == null) {
            throw new BadRequestException("There is no student with such id");
        }

        final Coursework coursework
                = courseworkRepository.findById(approvedCoursework.getCoursework().getId()).orElse(null);
        if (coursework == null) {
            throw new BadRequestException("There is no coursework with such id");
        }

        if (approvedCourseworkRepository.findByStudentAndCoursework(student, coursework) != null) {
            throw new BadRequestException("The student is already writing this course work");
        }

        if (places.getEngagedPlaces() >= places.getTotalPlaces()) {
            throw new BadRequestException("No free places");
        }

        places.setEngagedPlaces(places.getEngagedPlaces() + 1);
        placesRepository.save(places);

        approvedCoursework.setTeacher(user);
        return approvedCourseworkRepository.save(approvedCoursework).getId();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<ApprovedCoursework> getApprovedCourseworks(final String asRole, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        if (user.hasRole(asRole)) {
            if (asRole.equals("student")) {
                return approvedCourseworkRepository.findByStudent(user);
            }
            if (asRole.equals("teacher")) {
                return approvedCourseworkRepository.findByTeacher(user);
            }
        }

        return new ArrayList<>();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<ApprovedCoursework> getAll() {
        return approvedCourseworkRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public ApprovedCoursework getById(final Long id) {
        return approvedCourseworkRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final ApprovedCoursework approvedCoursework) {
        return approvedCourseworkRepository.save(approvedCoursework).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            approvedCourseworkRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
