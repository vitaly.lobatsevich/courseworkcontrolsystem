package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "directions")
@Data
@EqualsAndHashCode(callSuper = false)
public class Direction extends SQLEntity {
    @NotBlank
    @Column(name = "direction_name", nullable = false, unique = true)
    private String name;

}
