package by.vsu.courseworkcontrolsystem.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = IdNumberValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IdNumber {

    String message() default "Invalid id number format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
