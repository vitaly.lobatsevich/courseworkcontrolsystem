package by.vsu.courseworkcontrolsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestId implements Serializable {
    private static final long serialVersionUID = -8556930090451531001L;

    @Column(name = "student_id")
    protected Long studentId;

    @Column(name = "teacher_id")
    protected Long teacherId;

    @Column(name = "coursework_id")
    protected Long courseworkId;

}
