package by.vsu.courseworkcontrolsystem.repository;

import by.vsu.courseworkcontrolsystem.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {}
