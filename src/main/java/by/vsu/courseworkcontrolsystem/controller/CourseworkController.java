package by.vsu.courseworkcontrolsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import by.vsu.courseworkcontrolsystem.entity.Coursework;
import by.vsu.courseworkcontrolsystem.service.CourseworkService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/courseworks")
@AllArgsConstructor
public class CourseworkController {
    private final CourseworkService courseworkService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin') or hasAuthority('student') or hasAuthority('teacher')")
    public List<Coursework> getAll(@RequestParam final String as) {
        return courseworkService.getCourseworks(as, SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public Coursework getById(@PathVariable final Long id) {
        return courseworkService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long post(@RequestBody @Valid final Coursework coursework) {
        return courseworkService.save(coursework);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        courseworkService.deleteById(id);
    }

}
