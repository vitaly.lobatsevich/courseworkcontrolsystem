package by.vsu.courseworkcontrolsystem.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import by.vsu.courseworkcontrolsystem.entity.ContactInformation;
import by.vsu.courseworkcontrolsystem.service.ContactInformationService;
import lombok.AllArgsConstructor;

import java.util.List;

@RestController
@RequestMapping("/contact-information")
@AllArgsConstructor
public class ContactInformationController {
    private final ContactInformationService contactInformationService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<ContactInformation> getAll() {
        return contactInformationService.getAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public ContactInformation getById(@PathVariable final Long id) {
        return contactInformationService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long save(@RequestBody @Valid final ContactInformation contactInformation) {
        return contactInformationService.save(contactInformation);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        contactInformationService.deleteById(id);
    }

    @GetMapping("/me")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('student') or hasAuthority('teacher')")
    public ContactInformation getMe() {
        return contactInformationService
                .getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PostMapping("/me")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('student') or hasAuthority('teacher')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long post(@RequestBody @Valid final ContactInformation contactInformation) {
        return contactInformationService
                .saveForUsername(contactInformation, SecurityContextHolder.getContext().getAuthentication().getName());
    }

}
