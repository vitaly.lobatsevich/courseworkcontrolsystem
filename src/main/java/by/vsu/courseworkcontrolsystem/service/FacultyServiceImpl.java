package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Faculty;
import by.vsu.courseworkcontrolsystem.repository.FacultyRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class FacultyServiceImpl implements FacultyService {
    private final FacultyRepository facultyRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Faculty> getAll() {
        return facultyRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Faculty getById(final Long id) {
        return facultyRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Faculty faculty) {
        return facultyRepository.save(faculty).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            facultyRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
