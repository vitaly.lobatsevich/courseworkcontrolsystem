package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Request;
import by.vsu.courseworkcontrolsystem.entity.RequestId;

import java.util.List;

public interface RequestService extends CourseworkControlSystemService<Request, RequestId> {
    List<Request> getRequests(String asRole, String username);
    RequestId send(Request request, String username);
    void deleteByIdForUsername(RequestId id, String username);
}
