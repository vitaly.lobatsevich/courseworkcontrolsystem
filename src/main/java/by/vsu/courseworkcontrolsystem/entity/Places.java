package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Table(name = "places")
@Data
public class Places {
    @NotNull
    @EmbeddedId
    private PlacesId id;

    @NotNull
    @Column(name = "total_places", nullable = false)
    private Integer totalPlaces;

    @Column
    private Integer engagedPlaces;

    @ManyToOne
    @MapsId("teacher_id")
    @JoinColumn(name = "teacher_id")
    @JsonIgnoreProperties({ "places", "username", "password", "roles", "active" })
    private User user;

    @ManyToOne
    @MapsId("coursework_id")
    @JoinColumn(name = "coursework_id")
    @JsonIgnoreProperties({ "places", "group" })
    private Coursework coursework;

}
