package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "faculties")
@Data
@EqualsAndHashCode(callSuper = false)
public class Faculty extends SQLEntity {
    @NotBlank
    @Column(name = "faculty_name", nullable = false, unique = true)
    private String name;

}
