package by.vsu.courseworkcontrolsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseworkControlSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourseworkControlSystemApplication.class, args);
    }

}
