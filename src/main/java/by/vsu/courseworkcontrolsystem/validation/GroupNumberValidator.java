package by.vsu.courseworkcontrolsystem.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GroupNumberValidator implements ConstraintValidator<GroupNumber, String> {

    @Override
    public void initialize(GroupNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s == null || s.matches("\\d{2}");
    }

}
