package by.vsu.courseworkcontrolsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
    private static final long serialVersionUID = -7581826749868262839L;

    public BadRequestException() {
        super();
    }

    public BadRequestException(final String message) {
        super(message);
    }

}
