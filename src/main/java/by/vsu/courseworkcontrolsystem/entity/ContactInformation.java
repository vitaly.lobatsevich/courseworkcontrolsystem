package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import by.vsu.courseworkcontrolsystem.validation.PhoneNumber;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "contact_information")
@Data
@EqualsAndHashCode(callSuper = false)
public class ContactInformation extends SQLEntity {
    @PhoneNumber(message = "{contact-information.phone-number.phone-number}")
    @Column(name = "phone_number")
    private String phoneNumber;

    @Email(message = "{contact-information.email.email}")
    @Column(name = "email")
    private String email;

}
