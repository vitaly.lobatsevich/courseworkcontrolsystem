package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.Faculty;
import by.vsu.courseworkcontrolsystem.service.FacultyService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/faculties")
@AllArgsConstructor
public class FacultyController {
    private final FacultyService facultyService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<Faculty> getAll() {
        return facultyService.getAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public Faculty getById(@PathVariable final Long id) {
        return facultyService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long post(@RequestBody @Valid final Faculty faculty) {
        return facultyService.save(faculty);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        facultyService.deleteById(id);
    }

}
