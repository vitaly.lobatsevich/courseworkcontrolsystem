package by.vsu.courseworkcontrolsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import by.vsu.courseworkcontrolsystem.entity.Places;
import by.vsu.courseworkcontrolsystem.entity.PlacesId;
import by.vsu.courseworkcontrolsystem.service.PlacesService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/places")
@AllArgsConstructor
public class PlacesController {
    private final PlacesService placesService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<Places> getAll() {
        return placesService.getAll();
    }

    @GetMapping("/{teacherId}:{courseworkId}")
    @PreAuthorize("hasAuthority('admin')")
    public Places getById(@PathVariable final Long teacherId, @PathVariable final Long courseworkId) {
        PlacesId id = new PlacesId();
        id.setTeacherId(teacherId);
        id.setCourseworkId(courseworkId);
        return placesService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public PlacesId post(@RequestBody @Valid final Places places) {
        return placesService.save(places);
    }

    @DeleteMapping("/{teacherId}:{courseworkId}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long teacherId, @PathVariable final Long courseworkId) {
        placesService.deleteById(new PlacesId(teacherId, courseworkId));
    }

}
