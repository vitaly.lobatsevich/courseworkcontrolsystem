package by.vsu.courseworkcontrolsystem.service;

import java.util.List;

import javax.transaction.Transactional;

import by.vsu.courseworkcontrolsystem.entity.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import by.vsu.courseworkcontrolsystem.entity.Person;
import by.vsu.courseworkcontrolsystem.repository.PersonRepository;
import by.vsu.courseworkcontrolsystem.repository.UserRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {
    private final PersonRepository personRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Person> getAll() {
        return personRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Person getById(final Long id) {
        return personRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Person person) {
        return personRepository.save(person).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            personRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Person getByUsername(final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }
        return user.getPerson();
    }

}
