package by.vsu.courseworkcontrolsystem;

import by.vsu.courseworkcontrolsystem.entity.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.Date;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@SpringBootTest
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
public class MockMvcTests {
    private MockMvc mockMvc;

    @Autowired
    private FilterChainProxy filterChainProxy;

    private ObjectMapper mapper = new ObjectMapper();

    private static final String ADMIN_USERNAME = "0000000001";
    private static final String ADMIN_PASSWORD = "password";

    private static final String STUDENT_USERNAME = "0000000002";
    private static final String STUDENT_PASSWORD = "password";

    private static final String TEACHER_USERNAME = "0000000003";
    private static final String TEACHER_PASSWORD = "password";

    private static final String FACULTIES = "faculties";
    private static final String DEPARTMENTS = "departments";
    private static final String SPECIALTIES = "specialties";
    private static final String GROUPS = "groups";
    private static final String CONTACT_INFORMATION = "contact-information";
    private static final String DIRECTIONS = "directions";
    private static final String COURSEWORKS = "courseworks";
    private static final String PASSWORDS = "passwords";
    private static final String PERSONS = "persons";
    private static final String PLACES = "places";
    private static final String REQUESTS = "requests";
    private static final String ROLES = "roles";
    private static final String USERS = "users";
    private static final String APPROVED_COURSEWORKS = "approved-courseworks";

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation)
            throws Exception {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .addFilter(filterChainProxy)
                .build();
    }

    public String obtainAccessToken(final String username, final String password) throws Exception {
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", username);
        params.add("password", password);

        ResultActions result = mockMvc.perform(
                post("/oauth/token")
                        .with(httpBasic("dotnetclient", "password"))
                        .params(params)
                        .accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

        String resultString = result.andReturn().getResponse().getContentAsString();
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    private void getAll(final String collection, final String username, final String password) throws Exception {
        final String accessToken = obtainAccessToken(username, password);
        this.mockMvc.perform(
                get("/" + collection)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(collection + "_getAll"));
    }

    private void getById(final String collection, final Long id, final String username, final String password)
            throws Exception {
        final String accessToken = obtainAccessToken(username, password);
        this.mockMvc.perform(
                get("/" + collection + "/" + id)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(collection + "_getById"));
    }

    private void save(final String collection, final Object entity, final String username, final String password)
            throws Exception {
        final String accessToken = obtainAccessToken(username, password);
        this.mockMvc.perform(
                post("/" + collection)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(entity)).contentType("application/json;charset=UTF-8")
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isCreated())
                .andDo(document(collection + "_save"));
    }

    private void deleteById(final String collection, final Long id, final String username, final String password)
            throws Exception {
        final String accessToken = obtainAccessToken(username, password);
        this.mockMvc.perform(
                delete("/"  + collection + "/" + id)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(collection + "_deleteById"));
    }



    @Test
    public void getAllFaculties() throws Exception {
        getAll(FACULTIES, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getFacultyById() throws Exception {
        getById(FACULTIES, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveFaculty() throws Exception {
        final Faculty faculty = new Faculty();
        faculty.setName("Факультет обучения иностранных граждан");

        save(FACULTIES, faculty, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deleteFacultyById() throws Exception {
        deleteById(FACULTIES, 6L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void getAllDepartments() throws Exception {
        getAll(DEPARTMENTS, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getDepartmentById() throws Exception {
        getById(DEPARTMENTS, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveDepartment() throws Exception {
        final Department department = new Department();
        department.setName("Кафедра инженерной физики");
        final Faculty faculty = new Faculty();
        faculty.setId(1L);
        department.setFaculty(faculty);

        save(DEPARTMENTS, department, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deleteDepartmentById() throws Exception {
        deleteById(DEPARTMENTS, 6L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void getAllSpecialties() throws Exception {
        getAll(SPECIALTIES, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getSpecialtyById() throws Exception {
        getById(SPECIALTIES, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveSpecialty() throws Exception {
        final Specialty specialty = new Specialty();
        specialty.setName("Математика и информатика");
        final Faculty faculty = new Faculty();
        faculty.setId(1L);
        specialty.setFaculty(faculty);

        save(SPECIALTIES, specialty, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deleteSpecialtyById() throws Exception {
        deleteById(SPECIALTIES, 6L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void getAllGroups() throws Exception {
        getAll(GROUPS, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getGroupById() throws Exception {
        getById(GROUPS, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveGroup() throws Exception {
        final Group group = new Group();
        group.setNumber("21");
        group.setCourse(2);
        final Specialty specialty = new Specialty();
        specialty.setId(1L);
        group.setSpecialty(specialty);

        save(GROUPS, group, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deleteGroupById() throws Exception {
        deleteById(GROUPS, 6L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void getAllContactInformation() throws Exception {
        getAll(CONTACT_INFORMATION, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getContactInformationById() throws Exception {
        getById(CONTACT_INFORMATION, 1l, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveContactInformation() throws Exception {
        final ContactInformation contactInformation = new ContactInformation();
        save(CONTACT_INFORMATION, contactInformation, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deleteContactInformationById() throws Exception {
        deleteById(CONTACT_INFORMATION, 6L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getMeContactInformation() throws Exception {
        final String accessToken = obtainAccessToken(ADMIN_USERNAME, ADMIN_PASSWORD);
        this.mockMvc.perform(
                get("/contact-information/me")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document( "contact-information_getMe"));
    }

    @Test
    public void saveMeContactInformation() throws Exception {
        final String accessToken = obtainAccessToken(ADMIN_USERNAME, ADMIN_PASSWORD);

        final ContactInformation contactInformation = new ContactInformation();
        contactInformation.setEmail("super_e_mail@mail.ru");
        contactInformation.setPhoneNumber("+375(44)514-23-21");

        this.mockMvc.perform(
                post("/contact-information/me")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(contactInformation)).contentType("application/json;charset=UTF-8")
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isCreated())
                .andDo(document("contact-information_saveMe"));
    }



    @Test
    public void getAllDirections() throws Exception {
        getAll(DIRECTIONS, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getDirectionById() throws Exception {
        getById(DIRECTIONS, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveDirection() throws Exception {
        final Direction direction = new Direction();
        direction.setName("Администрирование и танцы с бубном");

        save(DIRECTIONS, direction, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deleteDirectionById() throws Exception {
        deleteById(DIRECTIONS, 6L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void getCourseworksAsAdmin() throws Exception {
        final String accessToken = obtainAccessToken(ADMIN_USERNAME, ADMIN_PASSWORD);
        this.mockMvc.perform(
                get("/" + COURSEWORKS + "?as=admin")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(COURSEWORKS + "_getAll_asAdmin"));
    }

    @Test
    public void getCourseworksAsStudent() throws Exception {
        final String accessToken = obtainAccessToken(STUDENT_USERNAME, STUDENT_PASSWORD);
        this.mockMvc.perform(
                get("/" + COURSEWORKS + "?as=student")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(COURSEWORKS + "_getAll_asStudent"));
    }

    @Test
    public void getCourseworksAsTeacher() throws Exception {
        final String accessToken = obtainAccessToken(TEACHER_USERNAME, TEACHER_PASSWORD);
        this.mockMvc.perform(
                get("/" + COURSEWORKS + "?as=teacher")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(COURSEWORKS + "_getAll_asTeacher"));
    }

    @Test
    public void getCourseworkById() throws Exception {
        getById(COURSEWORKS, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveCoursework() throws Exception {
        final Direction direction = new Direction();
        direction.setId(1L);

        final Group group = new Group();
        group.setId(1L);

        final Coursework coursework = new Coursework();
        coursework.setDirections(Collections.singletonList(direction));
        coursework.setGroup(group);
        coursework.setStartOfRegistration(new Date(System.currentTimeMillis()));
        coursework.setEndOfRegistration(new Date(System.currentTimeMillis()));

        save(COURSEWORKS, coursework, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deleteCourseworkById() throws Exception {
        deleteById(COURSEWORKS, 7L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void savePassword() throws Exception {
        final Password password = new Password();
        password.setPassword("NewPassword");

        save(PASSWORDS, password, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void saveMePassword() throws Exception {
        final String accessToken = obtainAccessToken(TEACHER_USERNAME, TEACHER_PASSWORD);

        final Password password = new Password();
        password.setPassword("teacher");

        this.mockMvc.perform(
                post("/passwords/me")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(password)).contentType("application/json;charset=UTF-8")
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isCreated())
                .andDo(document("passwords_saveMe"));
    }



    @Test
    public void getAllPersons() throws Exception {
        getAll(PERSONS, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getPersonById() throws Exception {
        getById(PERSONS, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getMePerson() throws Exception {
        final String accessToken = obtainAccessToken(STUDENT_USERNAME, STUDENT_PASSWORD);
        this.mockMvc.perform(
                get("/persons/me")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document( "persons_getMe"));
    }

    @Test
    public void savePerson() throws Exception {
        final Person person = new Person();
        person.setIdNumber("0000000009");
        person.setFirstName("Григорий");
        person.setLastName("Герасимов");
        person.setPatronymic("Иванович");

        save(PERSONS, person, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deletePersonById() throws Exception {
        deleteById(PERSONS, 7L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void getAllPlaces() throws Exception {
        getAll(PLACES, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getPlacesById() throws Exception {
        final String accessToken = obtainAccessToken(ADMIN_USERNAME, ADMIN_PASSWORD);
        this.mockMvc.perform(
                get("/places/3:1")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(PLACES + "_getById"));
    }

    @Test
    public void savePlaces() throws Exception {
        final PlacesId placesId = new PlacesId();
        placesId.setTeacherId(3L);
        placesId.setCourseworkId(4L);

        final Places places = new Places();
        places.setId(placesId);
        places.setTotalPlaces(3);

        save(PLACES, places, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void deletePlacesById() throws Exception {
        final String accessToken = obtainAccessToken(ADMIN_USERNAME, ADMIN_PASSWORD);
        this.mockMvc.perform(
                delete("/places/6:6")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(PLACES + "_deleteById"));
    }



    @Test
    public void getRequestsAsStudent() throws Exception {
        final String accessToken = obtainAccessToken(STUDENT_USERNAME, STUDENT_PASSWORD);
        this.mockMvc.perform(
                get("/" + REQUESTS + "?as=student")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(REQUESTS + "_getAll_asStudent"));
    }

    @Test
    public void getRequestsAsTeacher() throws Exception {
        final String accessToken = obtainAccessToken(TEACHER_USERNAME, TEACHER_PASSWORD);
        this.mockMvc.perform(
                get("/" + REQUESTS + "?as=teacher")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(REQUESTS + "_getAll_asTeacher"));
    }

    @Test
    public void saveRequest() throws Exception {
        final RequestId requestId = new RequestId();
        requestId.setTeacherId(3L);
        requestId.setCourseworkId(3L);

        final Request request = new Request();
        request.setId(requestId);
        request.setComment("Let's rock!");

        save(REQUESTS, request, STUDENT_USERNAME, STUDENT_PASSWORD);
    }

    @Test
    public void deleteRequestByIdAsStudent() throws Exception {
        final String accessToken = obtainAccessToken(TEACHER_USERNAME, TEACHER_PASSWORD);
        this.mockMvc.perform(
                delete("/"  + REQUESTS + "/2:3:1")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document(REQUESTS + "_deleteById"));
    }



    @Test
    public void getAllRoles() throws Exception {
        getAll(ROLES, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void getAllUsers() throws Exception {
        getAll(USERS, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getUserById() throws Exception {
        getById(USERS, 1L, ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void getMeUsername() throws Exception {
        final String accessToken = obtainAccessToken(STUDENT_USERNAME, STUDENT_PASSWORD);
        this.mockMvc.perform(
                get("/" + USERS + "/me")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+ accessToken)
        )
                .andExpect(status().isOk())
                .andDo(document( USERS + "_getMe"));
    }

    @Test
    public void saveUser() throws Exception {
        final Password password = new Password();
        password.setId(7L);

        final ContactInformation contactInformation = new ContactInformation();
        contactInformation.setId(8L);

        final Person person = new Person();
        person.setId(8L);

        final Role role = new Role();
        role.setId(2L);

        final Group group = new Group();
        group.setId(4L);

        final User user = new User();
        user.setUsername("0000000008");
        user.setPassword(password);
        user.setActive(true);
        user.setRoles(Collections.singletonList(role));
        user.setPerson(person);
        user.setContactInformation(contactInformation);
        user.setGroup(group);

        save(USERS, user, ADMIN_USERNAME, ADMIN_PASSWORD);
    }



    @Test
    public void saveApprovedCoursework() throws Exception {
        final User student = new User();
        student.setId(2L);

        final Coursework coursework = new Coursework();
        coursework.setId(2L);

        final ApprovedCoursework approvedCoursework = new ApprovedCoursework();
        approvedCoursework.setStudent(student);
        approvedCoursework.setCoursework(coursework);
        approvedCoursework.setTheme("Супер-пупер тема");

        save(APPROVED_COURSEWORKS, approvedCoursework, TEACHER_USERNAME, TEACHER_PASSWORD);
    }



}
