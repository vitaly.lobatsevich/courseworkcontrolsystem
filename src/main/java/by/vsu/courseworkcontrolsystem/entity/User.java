package by.vsu.courseworkcontrolsystem.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "users")
@Data
@EqualsAndHashCode(callSuper = false)
public class User extends SQLEntity {
    @NotBlank
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @NotNull
    @OneToOne
    @JoinColumn(name = "password_id", nullable = false, unique = true)
    @JsonIgnoreProperties("password")
    private Password password;

    @NotEmpty
    @ManyToMany
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Role> roles = new ArrayList<>();

    @Column(name = "active", nullable = false)
    private Boolean active = true;

    @NotNull
    @OneToOne
    @JoinColumn(name = "person_id", nullable = false, unique = true)
    private Person person;

    @NotNull
    @OneToOne
    @JoinColumn(name = "contact_information_id", nullable = false, unique = true)
    private ContactInformation contactInformation;

    @ManyToOne
    @JoinColumn(name = "group_id")
    @JsonIgnoreProperties({ "courseworks", "specialty" })
    private Group group;

    @ManyToMany
    @JoinTable(
            name = "teachers_departments",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "department_id")
    )
    @JsonIgnoreProperties("faculty")
    private List<Department> departments;

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    protected List<Places> places;

    public boolean hasRole(final Long id) {
        for (Role role : roles) {
            if (role.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasRole(final String name) {
        for (Role role : roles) {
            if (role.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

}
