package by.vsu.courseworkcontrolsystem.service;

import java.util.List;

import javax.transaction.Transactional;

import by.vsu.courseworkcontrolsystem.entity.Role;
import by.vsu.courseworkcontrolsystem.repository.RoleRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import by.vsu.courseworkcontrolsystem.entity.User;
import by.vsu.courseworkcontrolsystem.exception.BadRequestException;
import by.vsu.courseworkcontrolsystem.repository.UserRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public User getById(final Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public User getByUsername(final String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final User user) {
        for (Role role : user.getRoles()) {
            final Role dbRole = roleRepository.findById(role.getId() == null ? 0L : role.getId()).orElse(null);
            if (dbRole != null) {
                if (dbRole.getName().equals("student") && user.getGroup() == null) {
                    throw new BadRequestException("Field group cannot be null if user have student role");
                }
                if(dbRole.getName().equals("teacher")
                        && (user.getDepartments() == null || user.getDepartments().isEmpty())) {
                    throw new BadRequestException("The departments cannot be empty if user have teacher role");
                }
            }
        }
        return userRepository.save(user).getId();
    }

    @Override
    public void deleteById(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
