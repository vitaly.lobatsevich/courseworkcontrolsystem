package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import by.vsu.courseworkcontrolsystem.validation.IdNumber;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "persons")
@Data
@EqualsAndHashCode(callSuper = false)
public class Person extends SQLEntity {
    @NotBlank
    @IdNumber(message = "{person.id-number.id-number}")
    @Column(name = "id_number", nullable = false, unique = true)
    private String idNumber;

    @NotBlank
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotBlank
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

}
