package by.vsu.courseworkcontrolsystem.service;

import java.util.List;

public interface CourseworkControlSystemService<T, ID> {
    List<T> getAll();
    T getById(ID id);
    ID save(T t);
    void deleteById(ID id);
}
