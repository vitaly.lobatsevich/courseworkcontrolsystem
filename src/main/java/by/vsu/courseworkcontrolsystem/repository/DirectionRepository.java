package by.vsu.courseworkcontrolsystem.repository;

import by.vsu.courseworkcontrolsystem.entity.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectionRepository extends JpaRepository<Direction, Long> {}
