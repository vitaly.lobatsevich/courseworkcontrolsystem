package by.vsu.courseworkcontrolsystem.repository;

import by.vsu.courseworkcontrolsystem.entity.ApprovedCoursework;
import by.vsu.courseworkcontrolsystem.entity.Coursework;
import by.vsu.courseworkcontrolsystem.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApprovedCourseworkRepository extends JpaRepository<ApprovedCoursework, Long> {
    ApprovedCoursework findByStudentAndCoursework(User student, Coursework coursework);
    List<ApprovedCoursework> findByStudent(User student);
    List<ApprovedCoursework> findByTeacher(User teacher);
}
