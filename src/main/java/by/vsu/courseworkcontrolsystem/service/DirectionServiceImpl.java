package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Direction;
import by.vsu.courseworkcontrolsystem.repository.DirectionRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class DirectionServiceImpl implements DirectionService {
    private final DirectionRepository directionRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Direction> getAll() {
        return directionRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Direction getById(final Long id) {
        return directionRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Direction direction) {
        return directionRepository.save(direction).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            directionRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
