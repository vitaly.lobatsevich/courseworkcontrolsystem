package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.Specialty;
import by.vsu.courseworkcontrolsystem.service.SpecialtyService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/specialties")
@AllArgsConstructor
public class SpecialtyController {
    private final SpecialtyService specialtyService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<Specialty> getAll() {
        return specialtyService.getAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public Specialty getById(@PathVariable final Long id) {
        return specialtyService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long post(@RequestBody @Valid final Specialty specialty) {
        return specialtyService.save(specialty);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        specialtyService.deleteById(id);
    }

}
