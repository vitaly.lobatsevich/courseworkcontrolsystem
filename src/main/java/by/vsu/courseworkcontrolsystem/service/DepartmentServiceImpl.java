package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Department;
import by.vsu.courseworkcontrolsystem.repository.DepartmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {
    private final DepartmentRepository departmentRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Department> getAll() {
        return departmentRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Department getById(final Long id) {
        return departmentRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Department department) {
        return departmentRepository.save(department).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            departmentRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
