package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Role;
import by.vsu.courseworkcontrolsystem.repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Role getById(final Long id) {
        return roleRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Role role) {
        return roleRepository.save(role).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            roleRepository.deleteById(id);
        } catch(EmptyResultDataAccessException exception) {}
    }

}
