package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Password;

public interface PasswordService extends CourseworkControlSystemService<Password, Long> {
    public Long saveForUsername(Password password, String username);
}
