package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.Password;
import by.vsu.courseworkcontrolsystem.service.PasswordService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
// import java.util.List;

@RestController
@RequestMapping("/passwords")
@AllArgsConstructor
public class PasswordController {
    private final PasswordService passwordService;

    /*@GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<Password> getAll() {
        return passwordService.getAll();
    }*/

    /*@GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public Password getById(@PathVariable final Long id) {
        return passwordService.getById(id);
    }*/

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('admin')")
    public Long save(@RequestBody @Valid final Password password) {
        return passwordService.save(password);
    }

    /*@DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        passwordService.deleteById(id);
    }*/

    @PostMapping("/me")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('admin') or hasAuthority('student') or hasAuthority('teacher')")
    public Long updatePassword(@RequestBody @Valid final Password password) {
        return passwordService.saveForUsername(password,
                SecurityContextHolder.getContext().getAuthentication().getName());
    }

}
