package by.vsu.courseworkcontrolsystem.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import by.vsu.courseworkcontrolsystem.entity.*;
import by.vsu.courseworkcontrolsystem.repository.*;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import by.vsu.courseworkcontrolsystem.exception.BadRequestException;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class RequestServiceImpl implements RequestService {
    private final RequestRepository requestRepository;
    private final UserRepository userRepository;
    private final PlacesRepository placesRepository;
    private final ApprovedCourseworkRepository approvedCourseworkRepository;
    private final CourseworkRepository courseworkRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Request> getAll() {
        return requestRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Request getById(final RequestId id) {
        return requestRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public RequestId save(final Request request) {
        return requestRepository.save(request).getId();
    }

    @Override
    public void deleteById(final RequestId id) {
        try {
            requestRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Request> getRequests(final String asRole, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        if (user.hasRole(asRole)) {
            if (asRole.equals("student")) {
                return requestRepository.findByStudent(user);
            }
            if (asRole.equals("teacher")) {
                return requestRepository.findByTeacher(user);
            }
        }

        return new ArrayList<>();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public RequestId send(final Request request, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        final Coursework coursework
                = courseworkRepository.findById(request.getId().getCourseworkId()).orElse(null);
        if (coursework == null) {
            throw new BadRequestException("There is no coursework with such id");
        }

        final PlacesId id = new PlacesId();
        id.setTeacherId(request.getId().getTeacherId());
        id.setCourseworkId(request.getId().getCourseworkId());

        Places places = placesRepository.findById(id).orElse(null);
        if (places == null) {
            throw new BadRequestException("This teacher does not lead this course work");
        }
        if (!places.getCoursework().getGroup().getId().equals(user.getGroup().getId())) {
            throw new BadRequestException("There is no such coursework in this group");
        }
        if (places.getEngagedPlaces() >= places.getTotalPlaces()) {
            throw new BadRequestException("No free places");
        }
        final Date current = new Date(System.currentTimeMillis());
        if(current.before(places.getCoursework().getStartOfRegistration())
                || current.after(places.getCoursework().getEndOfRegistration())) {
            throw new BadRequestException("Registration has not started yet");
        }
        if (approvedCourseworkRepository.findByStudentAndCoursework(user, coursework) != null) {
            throw new BadRequestException("The student is already writing this course work");
        }

        request.getId().setStudentId(user.getId());
        return requestRepository.save(request).getId();
    }

    @Override
    public void deleteByIdForUsername(final RequestId id, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        if (user.getId().equals(id.getTeacherId()) || user.getId().equals(id.getStudentId())) {
            try {
                requestRepository.deleteById(id);
            } catch (EmptyResultDataAccessException exception) {}
        }
    }

}
