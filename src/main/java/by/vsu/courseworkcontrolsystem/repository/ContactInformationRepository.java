package by.vsu.courseworkcontrolsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import by.vsu.courseworkcontrolsystem.entity.ContactInformation;

@Repository
public interface ContactInformationRepository extends JpaRepository<ContactInformation, Long> {}
