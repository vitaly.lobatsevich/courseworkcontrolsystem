package by.vsu.courseworkcontrolsystem.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "roles")
@Data
@EqualsAndHashCode(callSuper = false)
public class Role extends SQLEntity {
    @NotBlank
    @Column(name = "role_name", nullable = false, unique = true)
    private String name;

}
