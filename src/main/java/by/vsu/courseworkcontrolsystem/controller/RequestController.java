package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.Request;
import by.vsu.courseworkcontrolsystem.entity.RequestId;
import by.vsu.courseworkcontrolsystem.service.RequestService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/requests")
@AllArgsConstructor
public class RequestController {
    private final RequestService requestService;

    @GetMapping
    @PreAuthorize("hasAuthority('student') or hasAuthority('teacher')")
    public List<Request> get(@RequestParam final String as) {
        return requestService.getRequests(as, SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PostMapping
    @PreAuthorize("hasAuthority('student')")
    @ResponseStatus(HttpStatus.CREATED)
    public RequestId post(@RequestBody @Valid final Request request) {
        return requestService.send(request, SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @DeleteMapping("/{studentId}:{teacherId}:{courseworkId}")
    @PreAuthorize("hasAuthority('student') or hasAuthority('teacher')")
    public void delete(
            @PathVariable final Long studentId,
            @PathVariable final Long teacherId,
            @PathVariable final Long courseworkId
    ) {
        requestService.deleteByIdForUsername(new RequestId(studentId, teacherId, courseworkId),
                SecurityContextHolder.getContext().getAuthentication().getName());
    }

}
