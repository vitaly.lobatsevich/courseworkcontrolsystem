package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.Direction;
import by.vsu.courseworkcontrolsystem.service.DirectionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/directions")
@AllArgsConstructor
public class DirectionController {
    private final DirectionService directionService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<Direction> getAll() {
        return directionService.getAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public Direction getById(@PathVariable final Long id) {
        return directionService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long post(@RequestBody @Valid final Direction direction) {
        return directionService.save(direction);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        directionService.deleteById(id);
    }

}
