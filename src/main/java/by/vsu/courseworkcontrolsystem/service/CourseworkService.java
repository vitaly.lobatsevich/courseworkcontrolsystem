package by.vsu.courseworkcontrolsystem.service;

import java.util.List;

import by.vsu.courseworkcontrolsystem.entity.Coursework;

public interface CourseworkService extends CourseworkControlSystemService<Coursework, Long> {
    List<Coursework> getCourseworks(String asRole, String username);
}
