package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Direction;

public interface DirectionService extends CourseworkControlSystemService<Direction, Long> {}
