package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "passwords")
@Data
@EqualsAndHashCode(callSuper = false)
public class Password extends SQLEntity {
    @NotBlank(message = "{password.password.not-blank}")
    @Column(name = "password", nullable = false)
    private String password;

}
