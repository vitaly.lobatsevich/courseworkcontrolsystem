package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Role;

public interface RoleService extends CourseworkControlSystemService<Role, Long> {}
