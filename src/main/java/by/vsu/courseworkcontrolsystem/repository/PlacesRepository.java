package by.vsu.courseworkcontrolsystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import by.vsu.courseworkcontrolsystem.entity.Places;
import by.vsu.courseworkcontrolsystem.entity.PlacesId;
import by.vsu.courseworkcontrolsystem.entity.User;

@Repository
public interface PlacesRepository extends JpaRepository<Places, PlacesId> {
    List<Places> findByUser(User user);
}
