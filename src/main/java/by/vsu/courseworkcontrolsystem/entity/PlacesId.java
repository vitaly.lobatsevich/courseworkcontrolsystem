package by.vsu.courseworkcontrolsystem.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlacesId implements Serializable {
    private static final long serialVersionUID = 2501238651149006073L;

    @Column(name = "teacher_id")
    private Long teacherId;

    @Column(name = "coursework_id")
    private Long courseworkId;

}
