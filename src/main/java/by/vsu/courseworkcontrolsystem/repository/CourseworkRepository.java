package by.vsu.courseworkcontrolsystem.repository;

import by.vsu.courseworkcontrolsystem.entity.Coursework;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseworkRepository extends JpaRepository<Coursework, Long> {}
