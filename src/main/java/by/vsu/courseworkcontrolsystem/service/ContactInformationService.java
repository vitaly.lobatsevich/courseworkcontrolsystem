package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.ContactInformation;

public interface ContactInformationService extends CourseworkControlSystemService<ContactInformation, Long> {
    ContactInformation getByUsername(String username);
    Long saveForUsername(ContactInformation contactInformation, String username);
}
