package by.vsu.courseworkcontrolsystem.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "courseworks")
@Data
@EqualsAndHashCode(callSuper = false)
public class Coursework extends SQLEntity {
    @NotEmpty
    @ManyToMany
    @JoinTable(
            name = "courseworks_directions",
            joinColumns = @JoinColumn(name = "coursework_id"),
            inverseJoinColumns = @JoinColumn(name = "direction_id")
    )
    private List<Direction> directions;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    private Group group;

    @NotNull
    @Column(name = "start_of_registration", nullable = false)
    private Date startOfRegistration;

    @NotNull
    @Column(name = "end_of_registration", nullable = false)
    private Date endOfRegistration;

    @OneToMany(mappedBy = "coursework")
    @JsonIgnoreProperties({ "coursework" })
    private List<Places> places;

}
