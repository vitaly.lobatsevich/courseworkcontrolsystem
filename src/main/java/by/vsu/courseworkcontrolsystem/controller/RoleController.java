package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.Role;
import by.vsu.courseworkcontrolsystem.service.RoleService;
import lombok.AllArgsConstructor;
// import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

// import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/roles")
@AllArgsConstructor
public class RoleController {
    private final RoleService roleService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<Role> getAll() {
        return roleService.getAll();
    }

    /*@GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public Role getById(@PathVariable final Long id) {
        return roleService.getById(id);
    }*/

    /*@PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long post(@RequestBody @Valid final Role role) {
        return roleService.save(role);
    }*/

    /*@DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        roleService.deleteById(id);
    }*/

}
