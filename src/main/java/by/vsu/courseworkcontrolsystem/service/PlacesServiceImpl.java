package by.vsu.courseworkcontrolsystem.service;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import by.vsu.courseworkcontrolsystem.entity.Places;
import by.vsu.courseworkcontrolsystem.entity.PlacesId;
import by.vsu.courseworkcontrolsystem.exception.BadRequestException;
import by.vsu.courseworkcontrolsystem.repository.PlacesRepository;
import lombok.AllArgsConstructor;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class PlacesServiceImpl implements PlacesService {
    private final PlacesRepository placesRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Places> getAll() {
        return placesRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Places getById(final PlacesId id) {
        return placesRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public PlacesId save(final Places places) {
        final Places dbPlaces = placesRepository.findById(places.getId()).orElse(null);
        if (dbPlaces != null) {
            if (dbPlaces.getEngagedPlaces() > places.getTotalPlaces()) {
                throw new BadRequestException("More engaged places than places total");
            }
            places.setEngagedPlaces(dbPlaces.getEngagedPlaces());
        } else {
            places.setEngagedPlaces(0);
        }
        return placesRepository.save(places).getId();
    }

    @Override
    public void deleteById(PlacesId id) {
        try {
            placesRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
