package by.vsu.courseworkcontrolsystem.repository;

import by.vsu.courseworkcontrolsystem.entity.Request;
import by.vsu.courseworkcontrolsystem.entity.RequestId;
import by.vsu.courseworkcontrolsystem.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<Request, RequestId> {
    List<Request> findByStudent(User student);
    List<Request> findByTeacher(User teacher);
}
