package by.vsu.courseworkcontrolsystem.repository;

import by.vsu.courseworkcontrolsystem.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {}
