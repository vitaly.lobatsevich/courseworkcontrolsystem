package by.vsu.courseworkcontrolsystem.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

    @Override
    public void initialize(PhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s == null ||
                s.matches("\\+\\d{3}-\\d{2}-\\d{3}-\\d{2}-\\d{2}") ||
                s.matches("\\+\\d{3}-\\d{2}-\\d{2}-\\d{2}-\\d{3}") ||
                s.matches("\\+\\d{3}\\(\\d{2}\\)\\d{3}-\\d{2}-\\d{2}") ||
                s.matches("\\+\\d{3}\\(\\d{2}\\)\\d{2}-\\d{2}-\\d{3}");
    }

}
