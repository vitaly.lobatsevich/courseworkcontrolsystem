package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Group;
import by.vsu.courseworkcontrolsystem.repository.GroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Group> getAll() {
        return groupRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Group getById(final Long id) {
        return groupRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Group group) {
        return groupRepository.save(group).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            groupRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
