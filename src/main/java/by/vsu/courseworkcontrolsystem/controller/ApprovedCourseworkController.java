package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.ApprovedCoursework;
import by.vsu.courseworkcontrolsystem.service.ApprovedCourseworkService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/approved-courseworks")
@AllArgsConstructor
public class ApprovedCourseworkController {
    private final ApprovedCourseworkService approvedCourseworkService;

    @GetMapping
    @PreAuthorize("hasAuthority('student') or hasAuthority('teacher')")
    public List<ApprovedCoursework> get(@RequestParam final String as) {
        return approvedCourseworkService
                .getApprovedCourseworks(as,SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('teacher')")
    public Long post(@RequestBody @Valid final ApprovedCoursework approvedCoursework) {
        return approvedCourseworkService
                .approve(approvedCoursework, SecurityContextHolder.getContext().getAuthentication().getName());
    }

}
