package by.vsu.courseworkcontrolsystem.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = GroupNumberValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface GroupNumber {

    String message() default "Invalid group number format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
