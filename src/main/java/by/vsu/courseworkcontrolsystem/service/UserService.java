package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.User;

public interface UserService extends CourseworkControlSystemService<User, Long> {
    User getByUsername(String username);
}
