package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Specialty;

public interface SpecialtyService extends CourseworkControlSystemService<Specialty, Long> {}
