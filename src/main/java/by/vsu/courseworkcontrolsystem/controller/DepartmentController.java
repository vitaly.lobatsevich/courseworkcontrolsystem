package by.vsu.courseworkcontrolsystem.controller;

import by.vsu.courseworkcontrolsystem.entity.Department;
import by.vsu.courseworkcontrolsystem.service.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/departments")
@AllArgsConstructor
public class DepartmentController {
    private final DepartmentService departmentService;

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    public List<Department> getAll() {
        return departmentService.getAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public Department getById(@PathVariable final Long id) {
        return departmentService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin')")
    @ResponseStatus(HttpStatus.CREATED)
    public Long post(@RequestBody @Valid final Department department) {
        return departmentService.save(department);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(@PathVariable final Long id) {
        departmentService.deleteById(id);
    }

}
