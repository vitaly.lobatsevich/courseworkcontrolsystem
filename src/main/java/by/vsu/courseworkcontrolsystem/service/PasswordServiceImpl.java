package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Password;
import by.vsu.courseworkcontrolsystem.entity.User;
import by.vsu.courseworkcontrolsystem.repository.PasswordRepository;
import by.vsu.courseworkcontrolsystem.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class PasswordServiceImpl implements PasswordService {
    private final PasswordRepository passwordRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Password> getAll() {
        return passwordRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Password getById(final Long id) {
        return passwordRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Password password) {
        password.setPassword(passwordEncoder.encode(password.getPassword()));
        return passwordRepository.save(password).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            passwordRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long saveForUsername(final Password password, final String username) {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        final Password current = user.getPassword();
        current.setPassword(passwordEncoder.encode(password.getPassword()));
        return passwordRepository.save(current).getId();
    }

}
