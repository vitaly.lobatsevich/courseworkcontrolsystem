package by.vsu.courseworkcontrolsystem.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Entity
@Table(name = "requests")
@Data
public class Request {
    @NotNull
    @EmbeddedId
    private RequestId id;

    @Column(name = "comment", nullable = false)
    private String comment;

    @ManyToOne
    @MapsId("student_id")
    @JoinColumn(name = "student_id")
    @JsonIgnoreProperties({ "password", "roles", "active", "username", "places" })
    private User student;

    @ManyToOne
    @MapsId("teacher_id")
    @JoinColumn(name = "teacher_id")
    @JsonIgnoreProperties({ "password", "roles", "active", "username", "places" })
    private User teacher;

    @ManyToOne
    @MapsId("coursework_id")
    @JoinColumn(name = "coursework_id")
    @JsonIgnoreProperties({ "places", "group" })
    private Coursework coursework;

}
