package by.vsu.courseworkcontrolsystem.service;

import by.vsu.courseworkcontrolsystem.entity.Specialty;
import by.vsu.courseworkcontrolsystem.repository.SpecialtyRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class SpecialtyServiceImpl implements SpecialtyService {
    private final SpecialtyRepository specialtyRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<Specialty> getAll() {
        return specialtyRepository.findAll();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Specialty getById(final Long id) {
        return specialtyRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Long save(final Specialty specialty) {
        return specialtyRepository.save(specialty).getId();
    }

    @Override
    public void deleteById(final Long id) {
        try {
            specialtyRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {}
    }

}
